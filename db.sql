create table books (
	id int auto_increment not null ,
    name varchar(255) COLLATE utf8_unicode_ci not null,
    publishier varchar(255) COLLATE utf8_unicode_ci not null,
    price bigint not null,
    primary key (id)
);

create table users (
	username varchar(255) not null,
    password varchar(255) not null,
    primary key (username)
);

insert into users values ('test123', 'test123');
insert into books values ('1', 'Đời ngắn đừng ngủ dài', 'Nhà xuất bản trẻ', '100000');
insert into books values ('2', 'Conan tập 1', 'Nhà xuất bản Kim Đồng', '30000');
insert into books values ('3', 'Doraemon', 'Nhà xuất bản Kim Đồng', '30000');
insert into books values ('4', 'Khéo ăn nói sẽ có được thiên hạ', 'Nhà xuất bản văn hóa', '99000');

