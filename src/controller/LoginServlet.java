package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;
import model.User;


public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO();
	
	public LoginServlet() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		String err = "";
		if (username.equals("") || password.equals("")) {
			err += "Phải nhập đầy đủ thông tin";
		}
		else {
			if (userDAO.login(username, password) == false) {
				err += "Tên tài khoản hoặc mật khẩu không chính xác!";
			}
		}
		
		if (err.length() > 0) {
			req.setAttribute("err", err);
		}
		
		String url = "/login.jsp";
		
		try {
			if (err.length() == 0) {
				userDAO.login(username, password);
				resp.sendRedirect("search.jsp");
				url = "/search.jsp";
			}
			else {
				url = "/login.jsp";
				RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
				rd.forward(req, resp);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			resp.sendRedirect("/login.jsp");
		}
	}
}

