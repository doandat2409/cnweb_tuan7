<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Search</title>
	</head>
	<body>
		<% String err=""; %>
		<h1>Tìm sách</h1>
		<li>
			<form accept-charset="utf8" method="post" action="SearchServlet" name="SearchServlet">
					<label>Tên sách</label>
					<br>
					<input accept-charset="utf-8" type="text" name="ten_sach" value="" placeholder="Search..."><br><br>
					<input type="submit" value="Search" name="timKiem">	
			</form>
		</li>
		<li style="color:red">
			<%=err %>
		</li>
	</body>
</html>